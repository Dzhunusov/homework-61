import React from 'react';
import './Countries.css';

const Countries = props => {
  return (
      <>
        <p className="country-item" onClick={props.clicked}>{props.name}</p>
      </>
  );
};

export default Countries;