import React, {useEffect, useState} from 'react';
import './CountryInfo.css';
import axios from "axios";

const COUNTRY_URL = 'https://restcountries.eu/rest/v2/alpha/';

const CountryInfo = props => {
  const [country, setCountry] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      if (props.code !== null) {
        const countryResponse = await axios.get(COUNTRY_URL + props.code);
        setCountry(countryResponse.data);
      }
    };
    fetchData().catch(console.error);
  }, [props.code]);

  return country && (
      <div>
        {props.code}
        <h3>{country.name}</h3>
        <p>Capital: {country.capital}</p>
        <p>Population: {country.population}</p>
        <img alt="#" src={country.flag} className="country-flag"/>
        <h4>Border with: </h4>
        <p>{country.borders.join('; ')}</p>
      </div>
  );
};

export default CountryInfo;