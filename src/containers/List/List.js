import React, {useEffect, useState} from 'react';
import './List.css';
import Countries from "../../components/Countries/Countries";
import axios from "axios";
import CountryInfo from "../../components/CountryInfo/CountryInfo";

const BASE_URL = 'https://restcountries.eu/rest/v2/alpha/';
const ALL_URL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
// const NAME_URL = 'name/';

const List = () => {
  const [countries, setCountries] = useState([]);
  const [selectedCountryCode, setSelectedCountryCode] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const countriesResponse = await axios.get(ALL_URL);
      const promises = countriesResponse.data.map(async country => {
        const countryUrl = BASE_URL + country.alpha3Code;
        const  countyNameResponse = await axios.get(countryUrl);
        return {
          ...country,
          name: countyNameResponse.data.name,
        };
      })
      const newCountry = await Promise.all(promises);
      setCountries(newCountry);
    }
    fetchData().catch(console.error);
  },[]);


  return (
      <>
        <div className="main-country-block">
          <section className="country-name">
            <h3>Choose country</h3>
            {countries.map(num => (
                <Countries
                    key={num.alpha3Code}
                    name={num.name}
                    clicked={() => setSelectedCountryCode(num.alpha3Code)}
                />
            ))}
          </section>
          <section className="country-info">
            <CountryInfo code={selectedCountryCode}/>
          </section>
        </div>
      </>
  );
};

export default List;